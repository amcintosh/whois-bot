import click
import configparser
import glob
import logging
import logging.config
import os
import sys
import frontmatter
from whoisbot.core import WhoisBot
from whoisbot.dao import UserDao

log = logging.getLogger(__name__)


@click.command()
@click.option("-c", "--config", default="./whois.ini",
              help="Path to the config file.")
def main(config):
    configs = load_config(config)
    log.info("Starting up.")
    try:
        WhoisBot(configs).start()
    except KeyboardInterrupt:  # pragma: no cover
        sys.exit(0)
    except Exception:
        log.exception("Fatal exception")


@click.command()
@click.option("-c", "--config", default="./whois.ini",
              help="Path to the config file.")
@click.option("-d", "--directory",
              default="/Users/amcintosh/work/statamic/_content/about/team/freshbookers",
              help="Path to the directory of staff markdown files.")
def data_fetcher(config, directory):
    '''Helper script to parse all the .md files in the statamic pulbic
    website project and spit the data into the db.
    '''
    configs = load_config(config)
    session = WhoisBot(configs).connect_db()
    dao = UserDao(session)
    files = glob.glob("{}/*.md".format(directory))
    for file in files:
        with open(file) as f:
            is_employed = not os.path.basename(file).startswith("_")
            person = frontmatter.load(f)
            user = dao.get_user_by_name(person["title"])
            if user and not is_employed and user.employed:
                log.info("User %s is no longer employed", person["title"])
                user.employed = False
                session.add(user)
                session.commit()
                continue
            elif user:
                log.debug("User already loaded: %s", person["title"])
                continue
            log.info("Loading user: %s", person["title"])
            user_info = {
                "name": person["title"],
                "title": person.get("job_title"),
                "photo_url": person["header_image"],
                "bio": person.content,
                "is_employed": is_employed
            }
            dao.save_new_user(user_info)


@click.command()
@click.option("-c", "--config", default="./whois.ini",
              help="Path to the config file.")
def missing_whois(config):
    '''Helper script to report all users that are incomplete.
    '''
    configs = load_config(config, load_logging=False)
    log.info("Running missing_whois")
    session = WhoisBot(configs).connect_db()
    users = UserDao(session).get_incomplete_users()
    for user in users:
        if user.employed:
            log.info(user.missing_string())


def load_config(config_file, load_logging=True):
    configs = configparser.ConfigParser()
    configs.read(config_file)

    if configs.get("DEFAULT", "LOG_CONFIG") and load_logging:
        logging.config.fileConfig(configs.get("DEFAULT", "LOG_CONFIG"))
    else:  # pragma: no cover
        logging.basicConfig(
            format="%(asctime)s - %(levelname)s: %(message)s",
            level=logging.DEBUG)
    log.debug("Config: %s", configs.defaults())
    return configs
