import logging
import statsdecor as stats
from statsdecor.decorators import timed
from whoisbot.dao import UserDao
from whoisbot.processors import Processor

log = logging.getLogger(__name__)


class FeedbackProcessor(Processor):

    @timed("processor.feedback.duration")
    def process(self, event):
        stats.incr("processor.feedback.run")
        self.channel_id = event["channel"]
        sending_user_id = event.get("user")
        feedback = event["text"].replace("feedback", "", 1).replace(
            "Feedback", "", 1).strip()

        log.info("%s sent feedback", sending_user_id)

        if not feedback:
            self.send_message("You didn't give me any feedback to send.\n"
                              "Tell me `feedback &lt;your feedback&gt;`")
            log.info("No feedback specified by %s", event["user"])
            return

        username = "unknown"
        dao = UserDao(self.session)
        user = dao.get_user_by_id(sending_user_id)
        if user:
            username = user.username

        admins = self.config.get("SLACK_ADMINS").split(",")
        message = "{} has feedback for whoisbot:\n```{}```".format(
            username, feedback)

        for admin in admins:
            channel = self.slack_client.server.channels.find(
                self.get_dm_channel(admin))
            log.info("Sending to admin '%s'", admin)
            self.send_message(message, channel)
