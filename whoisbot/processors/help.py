import logging
import statsdecor as stats
from statsdecor.decorators import timed
from whoisbot.processors import Processor

log = logging.getLogger(__name__)


class HelpProcessor(Processor):

    @timed("processor.help.duration")
    def process(self, event):
        stats.incr("processor.help.run")
        channel_id = event["channel"]
        log.debug("%s asked for help", event["user"])

        channel = self.slack_client.server.channels.find(channel_id)
        message = self._help_message()

        log.debug("Sending '%s' to '%s'", message, channel)
        if channel is not None:  # pragma: no branch
            channel.send_message(message)

    def _help_message(self):
        return "Hi! Here's what I can do:\n`help` - This here info\n" \
            "`who is &lt;@username&gt;` or `who is &lt;name&gt;` " \
            "- Ask me what I know about someone\n" \
            "`bio &lt;your bio&gt;` - Update your bio for others to see\n" \
            "`feedback &lt;feedback about me&gt;` - Send issues or feedback to me\n" \
            "You can direct message me or invite me to a channel and @me."
