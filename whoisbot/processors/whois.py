import logging
import re
import statsdecor as stats
from statsdecor.decorators import timed
from whoisbot.dao import UserDao
from whoisbot.processors import Processor
from whoisbot.utils import PERSON_URL

log = logging.getLogger(__name__)


class WhoisProcessor(Processor):

    @timed("processor.whois.duration")
    def process(self, event):
        stats.incr("processor.whois.run")
        channel_id = event["channel"]
        message = event.get("text").replace("<@{}> ".format(self.config["bot_id"]), "")
        requested_name = re.sub("[Ww]ho ?is ", "", message, 1)
        if requested_name[-1:] == "?":
            requested_name = requested_name[:-1]
        log.debug("%s asked who is %s", event["user"], requested_name)

        channel = self.slack_client.server.channels.find(channel_id)
        message = self.get_message(requested_name)

        log.debug("Sending '%s' to '%s'", message, channel)
        if channel is not None:  # pragma: no branch
            channel.send_message(message)

    def get_message(self, requested_name):
        dao = UserDao(self.session)

        reg = re.compile("<@([A-Za-z0-9]+)>")
        user_id_match = reg.search(requested_name)
        if user_id_match:
            user = dao.get_user_by_id(user_id_match.groups()[0])
        else:
            user = dao.get_user_by_name(requested_name)

            if not user:
                users = dao.get_user_by_unique_first_name(requested_name.split(" ")[0])
                if len(users) == 1:
                    user = users[0]
                elif len(users) > 0:
                    message = "Sorry, but there are {} people with that first name.".format(len(users))
                    for u in users:
                        message += "\n{}".format(u.name)
                    return message

        if not user:
            return "Sorry, but I don't know who that is."
        if user.employed:
            message = "Cool. Here's what I know about {} (@{})".format(
                user.name,
                user.username
            )
        else:
            message = "{} doesn't work here anymore, but here's what I know".format(user.name)

        if user.title:
            message += "\nTitle: {}".format(user.title)
        if user.bio:
            message += "\n```{}```".format(user.bio)
        if user.photo_url:
            message += "\n{}".format(PERSON_URL.format(user.photo_url))

        return message
