import logging

log = logging.getLogger(__name__)


class Processor:

    def __init__(self, config, slack_client, session):
        self.config = config
        self.slack_client = slack_client
        self.session = session

    def get_slack_user(self, user_id):
        call = self.slack_client.api_call("users.info", user=user_id)
        user = call["user"]
        return {
            "username": user["name"],
            "user_id": user_id,
            "name": user["profile"]["real_name"],
            "image": user["profile"]["image_192"],
            "email": user["profile"].get("email"),
            "title": user["profile"].get("title")
        }

    def get_dm_channel(self, user_id):
        call = self.slack_client.api_call("im.open", user=user_id)
        return call["channel"]["id"]

    def send_message(self, message, channel=None):
        if not channel:
            channel = self.slack_client.server.channels.find(self.channel_id)
        log.debug("Sending '%s' to '%s'", message, channel)
        if channel is not None:  # pragma: no branch
            channel.send_message(message)
