import logging
import statsdecor as stats
from statsdecor.decorators import timed
from whoisbot.dao import UserDao
from whoisbot.processors import Processor

log = logging.getLogger(__name__)


class BioProcessor(Processor):

    @timed("processor.bio.duration")
    def process(self, event):
        stats.incr("processor.bio.run")
        self.channel_id = event["channel"]
        sending_user_id = event.get("user")
        new_bio = event['text'].replace('bio', '', 1).replace('Bio', '', 1).strip()

        log.info("%s asked to change their bio", event["user"])

        if not new_bio:
            self.send_message(
                "You need to specify a bio.\nTell me `bio &lt;your bio&gt;`"
            )
            log.info("No bio specified by %s", event["user"])
            return

        dao = UserDao(self.session)
        user = dao.get_user_by_id(sending_user_id)
        if not user:
            self.send_message(
                "Oh dear. I don't actually know who you are, so I can't "
                "set your bio. Talk to @amcintosh to get this fixed."
            )
            log.info("%s attempted to update bio, but no record of user", event["user"])
            return

        user.bio = new_bio
        self.session.add(user)
        self.session.commit()

        self.send_message("Ok. Your bio is now:\n```{}```".format(new_bio))
