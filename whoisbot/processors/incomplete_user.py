import logging
import statsdecor as stats
from statsdecor.decorators import timed
from whoisbot.dao import UserDao
from whoisbot.processors import Processor
from whoisbot.utils import photo_finder

log = logging.getLogger(__name__)


class IncompleteUserProcessor(Processor):

    @timed("processor.incompleteuser.duration")
    def process(self, event):
        stats.incr("processor.incompleteuser.run")
        if event.get("presence") != "active":
            return
        log.debug("Saw presence: %s", event)
        self.dao = UserDao(self.session)
        user = self.dao.get_user_by_id(event["user"])

        if user and self.user_complete(user):
            return

        slack_user = self.get_slack_user(event["user"])

        if user:
            self.update_existing_user(user, slack_user)
            return

        user = self.dao.get_user_by_name(slack_user["name"])
        if user:
            self.update_existing_user(user, slack_user)
            return

        log.info("Saw unknown user %s (%s).",
                 slack_user["username"], slack_user["user_id"])

        if self.config.get("ADD_UNKNOWN_USERS"):
            self.dao.save_new_slack_user(slack_user)

    def user_complete(self, user):
        if user.username and user.slack_user_id and user.bio and \
                user.title and user.photo_url and user.active:
            return True
        return False

    def update_existing_user(self, user, slack_user):
        log.info("Saw incomplete user %s (%s). Updating.",
                 slack_user["username"], slack_user["user_id"])
        user.active = True
        user.slack_user_id = slack_user["user_id"]
        user.username = slack_user["username"]
        if not user.title:
            user.title = slack_user["title"]
        if not user.photo_url:
            user.photo_url = self.update_photo(user, slack_user)

        self.session.add(user)
        self.session.commit()

        if not user.bio:
            self.report_missing_bio(user, slack_user)

    def update_photo(self, user, slack_user):
        photo_url = photo_finder(user.name)
        if not photo_url:
            log.info("Unable to find staff photo for %s (%s): %s",
                     user.username, user.slack_user_id, user.name)
            return slack_user["image"]
        return photo_url

    def report_missing_bio(self, user, slack_user):
        log.info("%s (%s) has no bio", user.username, user.slack_user_id)
        if not self.config.get("SEND_DM_ON_MISSING_BIO"):
            return

        admins = self.config.get("SLACK_ADMINS").split(",")
        message = "{} has no bio".format(user.slack_user_id)
        for admin in admins:
            channel = self.slack_client.server.channels.find(self.get_dm_channel(admin))
            log.info("Sending '%s' to admin '%s'", message, admin)
            if channel is not None:  # pragma: no branch
                channel.send_message(message)
