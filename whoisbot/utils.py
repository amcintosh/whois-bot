import requests

PERSON_URL = 'https://www.freshbooks.com{}'


def photo_finder(full_name):
    '''Checks FreshBooks public website for the staff photo.
    We are no consistent with our naming, so we try an bunch of
    different image names.
    '''

    def first_initial_last(full_name):
        names = full_name.split(" ")
        if len(names) > 1:
            return names[0][0].lower() + names[1].lower()
        return full_name

    checks = [
        lambda name: "/assets/img/team/{}.jpg".format(name.replace(" ", "_")),
        lambda name: "/assets/img/team/{}.jpeg".format(name.replace(" ", "_")),
        lambda name: "/assets/img/team/{}.png".format(name.replace(" ", "_")),
        lambda name: "/assets/img/team/{}.jpg".format("_".join(reversed(name.split(" ")))),
        lambda name: "/assets/img/team/{}.jpeg".format("_".join(reversed(name.split(" ")))),
        lambda name: "/assets/img/team/{}.png".format("_".join(reversed(name.split(" ")))),
        lambda name: "/assets/img/team/{}.jpg".format(first_initial_last(name)),
        lambda name: "/assets/img/team/{}.jpeg".format(first_initial_last(name)),
        lambda name: "/assets/img/team/{}.png".format(first_initial_last(name)),
    ]

    for check in checks:
        image_name = check(full_name)
        res = requests.get(PERSON_URL.format(image_name))
        if res.status_code == 200:
            return image_name
    for check in checks:
        image_name = check(full_name.lower())
        res = requests.get(PERSON_URL.format(image_name))
        if res.status_code == 200:
            return image_name
    return None


if __name__ == "__main__":
    print(photo_finder("Alan Nowogrodski"))
