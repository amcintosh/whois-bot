from sqlalchemy import func
from whoisbot.models import User


class UserDao:

    def __init__(self, session):
        self.session = session

    def get_user_by_id(self, slack_user_id):
        return self.session.query(User).filter_by(
            slack_user_id=slack_user_id).first()

    def get_user_by_name(self, name):
        return self.session.query(User).filter(
            func.lower(User.name) == func.lower(name)).first()

    def get_user_by_unique_first_name(self, name, only_employed=True):
        query = self.session.query(User).filter(
            func.lower(User.name).like(func.lower(name + "%")))
        if only_employed:
            query = query.filter_by(employed=True)
        return query.all()

    def get_incomplete_users(self):
        return self.session.query(User).filter(
            (User.username == None) |
            (User.slack_user_id == None) |
            (User.bio == None) |
            (User.bio == '') |
            (User.title == None) |
            (User.title == '') |
            (User.photo_url == None)).all()

    def save_new_user(self, user_info):
        user = User(
            name=user_info["name"],
            title=user_info["title"],
            photo_url=user_info.get("photo_url"),
            bio=user_info.get("bio"),
            employed=user_info.get("is_employed")
        )
        self.session.add(user)
        self.session.commit()

    def save_new_slack_user(self, user_info):
        user = User(
            name=user_info["name"],
            slack_user_id=user_info["user_id"],
            username=user_info["username"],
            title=user_info["title"],
            photo_url=user_info.get("image"),
            active=True,
            updated_at=func.now()
        )
        self.session.add(user)
        self.session.commit()
