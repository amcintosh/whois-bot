from sqlalchemy import Column, Integer, String, Text, Boolean, TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.sql import func

Base = declarative_base()


class User(Base):
    __tablename__ = "user"

    id = Column(Integer, primary_key=True)
    name = Column(String(255))
    username = Column(String(255))
    slack_user_id = Column(String(255))
    title = Column(String(255))
    bio = Column(Text)
    temp_bio = Column(Text)
    photo_url = Column(String(255))
    active = Column(Boolean, default=False)
    employed = Column(Boolean, default=True)
    created_at = Column(TIMESTAMP, server_default=func.now())
    updated_at = Column(TIMESTAMP, onupdate=func.now())

    def __str__(self):  # pragma: no cover
        return self.name

    def missing_string(self):
        missing = []
        if not self.username:
            missing.append("username")
        if not self.slack_user_id:
            missing.append("slack_user_id")
        if not self.bio:
            missing.append("bio")
        if not self.title:
            missing.append("title")
        if not self.photo_url:
            missing.append("photo_url")
        return "{} ({}), missing: {}".format(self.name, self.id, ", ".join(missing))
