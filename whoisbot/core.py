import logging
import socket
import statsdecor
import time
from slackclient import SlackClient
from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from websocket import WebSocketConnectionClosedException, WebSocketTimeoutException
from whoisbot.models import Base
from whoisbot.processors.incomplete_user import IncompleteUserProcessor
from whoisbot.processors.bio import BioProcessor
from whoisbot.processors.feedback import FeedbackProcessor
from whoisbot.processors.help import HelpProcessor
from whoisbot.processors.whois import WhoisProcessor

log = logging.getLogger(__name__)


class WhoisBot:

    def __init__(self, config):
        self.config = config["DEFAULT"]
        self.session = self.connect_db()
        self.slack_client = SlackClient(self.config.get("SLACK_TOKEN"))

    def connect_db(self):
        path = self.config.get("DATABASE_PATH")
        engine = create_engine("sqlite:///" + path)
        Base.metadata.create_all(engine)
        session_factory = sessionmaker(bind=engine)
        Session = scoped_session(session_factory)
        return Session()

    def connect(self):
        self.slack_client.rtm_connect()
        bot_id = self.slack_client.server.login_data["self"]["id"]
        self.config["bot_id"] = bot_id
        log.debug("Connected. Bot id: %s", bot_id)

    def start(self):
        if self.config.get("DAEMON"):  # pragma: no cover
            import daemon
            with daemon.DaemonContext():
                self._start()
        else:
            self._start()

    def _start(self):
        statsdecor.configure(prefix='whoisbot')
        self.connect()
        while self.should_run():
            self.run()
            time.sleep(.1)

    def should_run(self):  # pragma: no cover
        return True

    def run(self):
        try:
            for message in self.slack_client.rtm_read():
                if message.get("user") == self.config.get("bot_id"):
                    pass
                elif message.get("type") == "message" and self.is_message_to_me(message):
                    self.process_message(message)
                elif message.get("type") == "presence_change":
                    IncompleteUserProcessor(self.config, self.slack_client, self.session).process(message)
        except (WebSocketConnectionClosedException, WebSocketTimeoutException, socket.timeout) as e:
            log.debug(e)
            self.connect()
        except Exception as e:
            log.exception(e)
            statsdecor.incr("slack.exception.unknown")

    def is_message_to_me(self, message):
        channel = message["channel"]
        text = message.get("text")
        if channel.startswith("D") or (
                text and text.startswith("<@{}>".format(self.config["bot_id"]))):
            return True
        return False

    def process_message(self, message):
        if "user" not in message:
            log.warn("message: %s", message)
            return
        log.info("%s just sent me: %s", message["user"], message.get("text"))
        statsdecor.incr("message.received")
        log.debug("message: %s", message)
        command = message.get("text").replace("<@{}> ".format(self.config["bot_id"]), "").lower()
        if command.startswith("whois") or command.startswith("who is"):
            WhoisProcessor(self.config, self.slack_client, self.session).process(message)
        elif command.startswith("bio"):
            BioProcessor(self.config, self.slack_client, self.session).process(message)
        elif command.startswith("feedback"):
            FeedbackProcessor(self.config, self.slack_client, self.session).process(message)
        else:
            HelpProcessor(self.config, self.slack_client, self.session).process(message)
