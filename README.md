Whois-bot
=========

Tell me about people on the FreshBooks team!

### Installation and Requirements

Whois-bot has been written for Python 3 (tested with on 3.5).

To install:
```
$ python setup.py install
```

### Setup

- Copy and rename `whois.ini.sample` to `whois.ini`. By default whois-bot will look for the file in the current directory (eg. `./whois.ini`) or you can specify the path at runtime (eg. `whois -c /path/to/whois.ini`). 

- Create a slack bot and add the token to `SLACK_TOKEN = <YOUR TOKEN>` in `whois.ini`

- Add a list of admin slack user ids `SLACK_ADMINS = U000XXXX,U000YYYY`

- Service scripts can be found in `/scripts`

#### Server Setup

- `sudo mkdir /var/log/whoisbot`

    `sudo chown andrew:andrew /var/log/whoisbot/`

- Put the following in `/etc/systemd/system/whoisbot.service.d/myenv.conf`

```
#!ini

[Service]
Environment="LANG=C.UTF-8"
```

- `sudo systemctl start whoisbot`