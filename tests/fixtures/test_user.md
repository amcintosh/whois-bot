---
title: Susan Ivanova
job_title: Second in Command
header_image: /assets/img/team/sivanova.jpg
---
No boom today. Boom tomorrow. There's always a boom tomorrow.