import datetime
import factory
from whoisbot import models


class UserFactory(factory.alchemy.SQLAlchemyModelFactory):
    class Meta:
        model = models.User

    id = factory.Sequence(lambda n: n)
    name = "Michael Garibaldi"
    username = "mgaribaldi"
    slack_user_id = "U00000XXX"
    title = "Chief of Security"
    bio = "Has 2 favourite things in the universe."
    photo_url = "/some/place.png"
    active = True
    employed = True
    created_at = datetime.datetime.now()
    updated_at = datetime.datetime.now()
