import os


DEFAULT_CONFIG = {
    "DEFAULT": {
        "DATABASE_PATH": "",
        "LOG_CONFIG": "./logging-test.ini",
        "SLACK_TOKEN": "some_token",
        "SLACK_ADMINS": "U0000ZZZ",
        "SLACK_BOT_NAME": "whois-bot",
        "SEND_DM_ON_MISSING_BIO": True
    }
}


class DbTest:

    def teardown_method(self, method):
        test_db = DEFAULT_CONFIG["DEFAULT"]["DATABASE_PATH"]
        if os.path.exists(test_db):
            os.unlink(test_db)
