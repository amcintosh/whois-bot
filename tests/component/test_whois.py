from unittest.mock import MagicMock, Mock, patch
from whoisbot.core import WhoisBot
from tests import DEFAULT_CONFIG, DbTest
from tests.component import mock_runner, mock_slack_calls, mock_slack_channel_find
from tests.factories import UserFactory


class TestWhoisProcessor(DbTest):

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_matched_by_slack_id(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is <@U00000XXX>"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "U00000ZZZ just sent me: who is <@U00000XXX>" in caplog.text()
        assert "U00000ZZZ asked who is <@U00000XXX>" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Cool. Here's what I know about Michael Garibaldi (@mgaribaldi)\n"
            "Title: Chief of Security\n```Has 2 favourite things in the universe."
            "```\nhttps://www.freshbooks.com/some/place.png"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_matched_by_name(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael Garibaldi"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "U00000ZZZ just sent me: who is Michael Garibaldi" in caplog.text()
        assert "U00000ZZZ asked who is Michael Garibaldi" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Cool. Here's what I know about Michael Garibaldi (@mgaribaldi)\n"
            "Title: Chief of Security\n```Has 2 favourite things in the universe."
            "```\nhttps://www.freshbooks.com/some/place.png"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_matched_by_first_name(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "U00000ZZZ just sent me: who is Michael" in caplog.text()
        assert "U00000ZZZ asked who is Michael" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Cool. Here's what I know about Michael Garibaldi (@mgaribaldi)\n"
            "Title: Chief of Security\n```Has 2 favourite things in the universe."
            "```\nhttps://www.freshbooks.com/some/place.png"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_matched_by_first_name_and_initial(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael G"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "U00000ZZZ just sent me: who is Michael G" in caplog.text()
        assert "U00000ZZZ asked who is Michael G" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Cool. Here's what I know about Michael Garibaldi (@mgaribaldi)\n"
            "Title: Chief of Security\n```Has 2 favourite things in the universe."
            "```\nhttps://www.freshbooks.com/some/place.png"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_no_user_found(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael Garibaldi"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)
        bot.start()

        assert "U00000ZZZ just sent me: who is Michael Garibaldi" in caplog.text()
        assert "U00000ZZZ asked who is Michael Garibaldi" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with("Sorry, but I don't know who that is.")

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_too_many_first_names_found(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael G"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()
        UserFactory.create(
            name="Michael Else",
            username="melse",
            slack_user_id="U00000XXX2"
        )

        bot.start()

        assert "U00000ZZZ just sent me: who is Michael G" in caplog.text()
        assert "U00000ZZZ asked who is Michael G" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Sorry, but there are 2 people with that first name."
            "\nMichael Garibaldi"
            "\nMichael Else"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_multiple_first_names_show_only_employed(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael G"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()
        UserFactory.create(
            name="Michael Else",
            username="melse",
            slack_user_id="U00000XXX2",
            employed=False
        )

        bot.start()

        assert "U00000ZZZ just sent me: who is Michael G" in caplog.text()
        assert "U00000ZZZ asked who is Michael G" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Cool. Here's what I know about Michael Garibaldi (@mgaribaldi)\n"
            "Title: Chief of Security\n```Has 2 favourite things in the universe."
            "```\nhttps://www.freshbooks.com/some/place.png"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_not_employed(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "who is Michael Garibaldi"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create(employed=False)

        bot.start()

        assert "U00000ZZZ just sent me: who is Michael Garibaldi" in caplog.text()
        assert "U00000ZZZ asked who is Michael Garibaldi" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Michael Garibaldi doesn't work here anymore, but here's what I know\n"
            "Title: Chief of Security\n```Has 2 favourite things in the universe."
            "```\nhttps://www.freshbooks.com/some/place.png"
        )
