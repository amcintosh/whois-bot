import copy
from unittest.mock import MagicMock, Mock, patch
from whoisbot.core import WhoisBot
from tests import DEFAULT_CONFIG, DbTest
from tests.component import mock_runner, mock_slack_calls, mock_slack_channel_find
from tests.factories import UserFactory


class TestIncompleteUserProcessor(DbTest):

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_not_active_status(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "away"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read

        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)
        bot.start()

        assert "Saw presence:" not in caplog.text()
        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user" not in caplog.text()

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_no_user(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read

        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)
        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." in caplog.text()
        assert "Saw incomplete user" not in caplog.text()

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_complete_user(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read

        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user" not in caplog.text()

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_inactive_user(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read

        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        user = UserFactory.create(active=False)

        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user mgaribaldi (U00000XXX)." in caplog.text()
        assert user.active is True

    @patch("whoisbot.processors.incomplete_user.photo_finder", auto_spec=True)
    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_incomplete_user(self, mock_should_run, mock_slack, mock_photo_finder, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_photo_finder.return_value = None
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        user = UserFactory.create(
            slack_user_id=None,
            title=None,
            photo_url=None,
            active=False
        )

        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user mgaribaldi (U00000XXX)." in caplog.text()
        assert "Unable to find staff photo for mgaribaldi (U00000XXX): Michael Garibaldi" in caplog.text()
        assert user.slack_user_id == "U00000XXX"
        assert user.title == "Chief of Security"
        assert user.photo_url == "http://a_image.png"
        assert user.active is True

    @patch("whoisbot.processors.incomplete_user.photo_finder", auto_spec=True)
    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_incomplete_user_found_photo(self, mock_should_run, mock_slack, mock_photo_finder, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_photo_finder.return_value = "http://found_photo.png"
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        user = UserFactory.create(
            name="Michael Garibaldi",
            slack_user_id=None,
            photo_url=None,
        )

        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user mgaribaldi (U00000XXX)." in caplog.text()
        assert "Unable to find staff photo for mgaribaldi (U00000XXX): Michael Garibaldi" not in caplog.text()
        assert user.photo_url == "http://found_photo.png"

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_missing_bio_log(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )
        config = copy.deepcopy(DEFAULT_CONFIG)
        del config["DEFAULT"]["SEND_DM_ON_MISSING_BIO"]
        bot = WhoisBot(config)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create(bio=None)

        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user mgaribaldi (U00000XXX)." in caplog.text()
        assert "mgaribaldi (U00000XXX) has no bio" in caplog.text()

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_missing_bio_send(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "presence_change",
            "presence": "active"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create(bio=None)

        bot.start()

        assert "Saw unknown user mgaribaldi (U00000XXX)." not in caplog.text()
        assert "Saw incomplete user mgaribaldi (U00000XXX)." in caplog.text()
        assert "mgaribaldi (U00000XXX) has no bio" in caplog.text()
        mock_channel_find.assert_called_with("D00AD1BBB")
        mock_send.assert_called_with(
            "U00000XXX has no bio"
        )
