from unittest.mock import Mock, PropertyMock


def mock_runner():
    list_of_return_values = [False, True]

    def side_effect():
        return list_of_return_values.pop()
    return side_effect


def mock_slack_calls(call, user):
    if call == "users.info":
        return {
            "user": {
                "user_id": user,
                "name": "mgaribaldi",
                "profile": {
                    "real_name": "Michael Garibaldi",
                    "image_192": "http://a_image.png",
                    "email": "mgaribaldi@b5.net",
                    "title": "Chief of Security"
                }
            }
        }
    elif call == "im.open":
        return {"channel": {"id": "D00AD1BBB"}}


def mock_slack_channel_find(mock_slack):
    mock_channel_find = Mock()
    mock_channels = PropertyMock()
    mock_server = PropertyMock()
    mock_channels.find = mock_channel_find
    mock_server.channels = mock_channels
    mock_slack.return_value.server = mock_server

    return mock_channel_find
