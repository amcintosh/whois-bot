import copy
import os
from unittest.mock import patch
from click.testing import CliRunner
from whoisbot import cli
from whoisbot.core import WhoisBot
from whoisbot.models import User
from tests import DEFAULT_CONFIG, DbTest
from tests.factories import UserFactory


class TestCli(DbTest):

    @patch("whoisbot.cli.logging", autospec=True)
    @patch("whoisbot.cli.WhoisBot", autospec=True)
    def test_main_runs(self, mock_bot, mock_logging, caplog):
        runner = CliRunner()
        result = runner.invoke(cli.main, ["--config", "./tests/test.ini"])

        assert mock_bot.return_value.start.called
        mock_logging.config.fileConfig.assert_called_once_with("./logging-dev.ini")
        assert "Starting up." in caplog.text()
        assert result.exit_code == 0

    @patch("whoisbot.cli.logging", autospec=True)
    @patch("whoisbot.cli.load_config")
    def test_data_fetcher(self, mock_config, mock_logging, caplog):
        config = copy.deepcopy(DEFAULT_CONFIG)
        config["DEFAULT"]["DATABASE_PATH"] = "./test.db"

        mock_config.return_value = config
        runner = CliRunner()
        result = runner.invoke(
            cli.data_fetcher,
            ["--config", "./tests/test.ini", "--directory", "./tests/fixtures/"]
        )

        bot = WhoisBot(config)
        user = bot.session.query(User).first()

        if os.path.exists(config["DEFAULT"]["DATABASE_PATH"]):
            os.unlink(config["DEFAULT"]["DATABASE_PATH"])

        assert "Loading user: Susan Ivanova" in caplog.text()
        assert user.name == "Susan Ivanova"
        assert user.title == "Second in Command"
        assert user.photo_url == "/assets/img/team/sivanova.jpg"
        assert user.bio == "No boom today. Boom tomorrow. There's always a boom tomorrow."
        assert user.employed is True
        assert result.exit_code == 0

    @patch("whoisbot.cli.logging", autospec=True)
    @patch("whoisbot.cli.load_config")
    def test_data_fetcher_user_exists(self, mock_config, mock_logging, caplog):
        config = copy.deepcopy(DEFAULT_CONFIG)
        config["DEFAULT"]["DATABASE_PATH"] = "./test.db"

        mock_config.return_value = config

        runner = CliRunner()
        result = runner.invoke(
            cli.data_fetcher,
            ["--config", "./tests/test.ini", "--directory", "./tests/fixtures/"]
        )

        assert "Loading user: Susan Ivanova" in caplog.text()
        assert result.exit_code == 0

        result = runner.invoke(
            cli.data_fetcher,
            ["--config", "./tests/test.ini", "--directory", "./tests/fixtures/"]
        )

        bot = WhoisBot(config)
        user = bot.session.query(User).first()

        if os.path.exists(config["DEFAULT"]["DATABASE_PATH"]):
            os.unlink(config["DEFAULT"]["DATABASE_PATH"])

        assert "User already loaded: Susan Ivanova" in caplog.text()
        assert user.name == "Susan Ivanova"
        assert user.title == "Second in Command"
        assert user.photo_url == "/assets/img/team/sivanova.jpg"
        assert user.bio == "No boom today. Boom tomorrow. There's always a boom tomorrow."
        assert result.exit_code == 0

    @patch("whoisbot.cli.logging", autospec=True)
    @patch("whoisbot.cli.load_config")
    def test_employee_leaves(self, mock_config, mock_logging, caplog):
        config = copy.deepcopy(DEFAULT_CONFIG)
        config["DEFAULT"]["DATABASE_PATH"] = "./test.db"

        mock_config.return_value = config
        bot = WhoisBot(config)
        UserFactory._meta.sqlalchemy_session = bot.session

        UserFactory.create()
        bot.session.commit()

        runner = CliRunner()
        result = runner.invoke(
            cli.data_fetcher,
            ["--config", "./tests/test.ini", "--directory", "./tests/fixtures/sad/"]
        )

        if os.path.exists(config["DEFAULT"]["DATABASE_PATH"]):
            os.unlink(config["DEFAULT"]["DATABASE_PATH"])

        assert "User Michael Garibaldi is no longer employed" in caplog.text()
        assert result.exit_code == 0

    @patch("whoisbot.cli.logging", autospec=True)
    @patch("whoisbot.cli.load_config")
    def test_missing_whois(self, mock_config, mock_logging, caplog):
        config = copy.deepcopy(DEFAULT_CONFIG)
        config["DEFAULT"]["DATABASE_PATH"] = "./test.db"

        mock_config.return_value = config
        bot = WhoisBot(config)
        UserFactory._meta.sqlalchemy_session = bot.session

        UserFactory.create()
        UserFactory.create(
            name="Susan Ivanova",
            username=None,
            slack_user_id="U00000XXY",
            title="Second in Command",
            photo_url="/assets/img/team/sivanova.jpg",
            bio="No boom today. Boom tomorrow. There's always a boom tomorrow."
        )
        UserFactory.create(
            name="Londo Mollari",
            username="lmollari",
            slack_user_id=None,
            title="Ambassador",
            photo_url=None,
            bio="What do you want, you moon-faced assassin of joy?"
        )
        UserFactory.create(
            name="G'Kar",
            username="gkar",
            slack_user_id=None,
            title="Ambassador",
            photo_url=None,
            employed=False,
            bio="No one here is exactly what he appears."
        )
        bot.session.commit()

        runner = CliRunner()
        result = runner.invoke(cli.missing_whois, ["--config", "./tests/test.ini"])

        if os.path.exists(config["DEFAULT"]["DATABASE_PATH"]):
            os.unlink(config["DEFAULT"]["DATABASE_PATH"])

        assert "Running missing_whois" in caplog.text()
        assert "Susan Ivanova (4), missing: username" in caplog.text()
        assert "Londo Mollari (5), missing: slack_user_id, photo_url" in caplog.text()
        assert "G'Kar (6), missing:" not in caplog.text()
        assert result.exit_code == 0
