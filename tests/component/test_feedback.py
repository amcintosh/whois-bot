from unittest.mock import MagicMock, Mock, patch
from whoisbot.core import WhoisBot
from tests import DEFAULT_CONFIG, DbTest
from tests.component import mock_runner, mock_slack_calls, mock_slack_channel_find
from tests.factories import UserFactory


class TestFeedbackProcessor(DbTest):

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_sends_feedback(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "message",
            "channel": "D00002BBB",
            "text": "feedback I like this thing"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "U00000XXX just sent me: feedback I like this thing" in caplog.text()
        assert "U00000XXX sent feedback" in caplog.text()
        assert "Sending to admin 'U0000ZZZ'" in caplog.text()
        mock_channel_find.assert_called_once_with("D00AD1BBB")
        mock_send.assert_called_once_with(
            "mgaribaldi has feedback for whoisbot:\n```I like this thing```"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_sends_no_feedback(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "message",
            "channel": "D00002BBB",
            "text": "feedback "
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        UserFactory.create()

        bot.start()

        assert "U00000XXX just sent me: feedback " in caplog.text()
        assert "U00000XXX sent feedback" in caplog.text()
        assert "No feedback specified by U00000XXX" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "You didn't give me any feedback to send.\n"
            "Tell me `feedback &lt;your feedback&gt;`"
        )

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_unknown_user_sends_feedback(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "message",
            "channel": "D00002BBB",
            "text": "feedback I like this thing"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        WhoisBot(DEFAULT_CONFIG).start()

        assert "U00000XXX just sent me: feedback I like this thing" in caplog.text()
        assert "U00000XXX sent feedback" in caplog.text()
        assert "Sending to admin 'U0000ZZZ'" in caplog.text()
        mock_channel_find.assert_called_once_with("D00AD1BBB")
        mock_send.assert_called_once_with(
            "unknown has feedback for whoisbot:\n```I like this thing```"
        )
