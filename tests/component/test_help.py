from unittest.mock import MagicMock, Mock, patch
from whoisbot.core import WhoisBot
from tests import DEFAULT_CONFIG
from tests.component import mock_runner, mock_slack_calls, mock_slack_channel_find


class TestHelpProcessor():

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_matched_by_slack_id(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000ZZZ",
            "type": "message",
            "channel": "D00002BBB",
            "text": "help"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)
        bot.start()

        assert "U00000ZZZ just sent me: help" in caplog.text()
        assert "U00000ZZZ asked for help" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Hi! Here's what I can do:\n`help` - This here info\n"
            "`who is &lt;@username&gt;` or `who is &lt;name&gt;` "
            "- Ask me what I know about someone\n"
            "`bio &lt;your bio&gt;` - Update your bio for others to see\n"
            "`feedback &lt;feedback about me&gt;` - Send issues or feedback to me\n"
            "You can direct message me or invite me to a channel and @me."
        )
