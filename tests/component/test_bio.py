from unittest.mock import MagicMock, Mock, patch
from whoisbot.core import WhoisBot
from tests import DEFAULT_CONFIG, DbTest
from tests.component import mock_runner, mock_slack_calls, mock_slack_channel_find
from tests.factories import UserFactory


class TestBioProcessor(DbTest):

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_updates_bio(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "message",
            "channel": "D00002BBB",
            "text": "bio My new bio"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        user = UserFactory.create()

        bot.start()

        assert "U00000XXX just sent me: bio My new bio" in caplog.text()
        assert "U00000XXX asked to change their bio" in caplog.text()
        assert user.bio == "My new bio"
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with("Ok. Your bio is now:\n```My new bio```")

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_user_sends_no_bio(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "message",
            "channel": "D00002BBB",
            "text": "bio"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)

        UserFactory._meta.sqlalchemy_session = bot.session
        user = UserFactory.create()

        bot.start()

        assert "U00000XXX just sent me: bio" in caplog.text()
        assert "No bio specified by U00000XXX" in caplog.text()
        assert user.bio == "Has 2 favourite things in the universe."
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with("You need to specify a bio.\nTell me `bio &lt;your bio&gt;`")

    @patch("whoisbot.core.SlackClient", auto_spec=True)
    @patch("whoisbot.core.WhoisBot.should_run")
    def test_missing_tries_bio_update(self, mock_should_run, mock_slack, caplog):
        mock_should_run.side_effect = mock_runner()
        mock_rtm_read = MagicMock()
        mock_rtm_read.__iter__.return_value = [{
            "user": "U00000XXX",
            "type": "message",
            "channel": "D00002BBB",
            "text": "bio My new bio"
        }, ]
        mock_slack.return_value.rtm_read.return_value = mock_rtm_read
        mock_channel_find = mock_slack_channel_find(mock_slack)
        mock_send = Mock()
        mock_channel_find.return_value.send_message = mock_send
        mock_slack.return_value.api_call = MagicMock(
            side_effect=mock_slack_calls
        )

        bot = WhoisBot(DEFAULT_CONFIG)
        bot.start()

        assert "U00000XXX just sent me: bio" in caplog.text()
        assert "U00000XXX attempted to update bio, but no record of user" in caplog.text()
        mock_channel_find.assert_called_once_with("D00002BBB")
        mock_send.assert_called_once_with(
            "Oh dear. I don't actually know who you are, so I can't "
            "set your bio. Talk to @amcintosh to get this fixed."
        )
