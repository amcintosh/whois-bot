from setuptools import setup, find_packages
import whoisbot

setup(
    name="whoIsBot",
    version=whoisbot.__version__,
    author=whoisbot.__author__,
    author_email="andrew@amcintosh.net",
    description="Slack bot for looking up colleagues",
    license=whoisbot.__license__,
    packages=find_packages(exclude=["*.test", "*.test.*"]),
    include_package_data=True,
    install_requires=open("requirements.txt").readlines(),
    setup_requires=['pytest-runner', ],
    tests_require=['pytest', ],
    entry_points={
        "console_scripts": [
            "whoisbot=whoisbot.cli:main",
            "data_fetch=whoisbot.cli:data_fetcher",
            "missing_whois=whoisbot.cli:missing_whois"
        ]
    },
    test_suite="tests"
)
